all: setup build clean

project = rev00016_wpg

setup:
	mkdir -p .build
	cp -rv \
		include \
	  *.tex \
	  .build

build:
	cd .build && \
	  latexmk -pdf frame.tex && \
	  cp frame.pdf ..
	ln -sf frame.pdf $(project).pdf

clean:
	rm -rf .build
