% WildFuck polynomials
-module(wf).

-export(
    [ aob2wf/1
    , baseprod/1
    , lprod/1
    , lsum/1
    , reduce/1
    , sym/1
    ]
).

% A leaf
-type baseprod() :: uno
                  | {baseprod, [atom()]}.
-type sum(T)     :: {sum, T, T}.
-type prod(T)    :: {prod, T, T}.


% binary tree, two types of stems. products of atoms are considered leaves
-type aob_tree() :: baseprod()
                  | sum( aob_tree())
                  | prod(aob_tree()).

% This is good because reduced data looks different to the program than
% non-reduced data
%
% summation is implied
%
% Wildfuck = sum of baseprods
-type wf()      :: {wf, [baseprod()]}.

%-----------------------------------------------------------------------------%
% API
%-----------------------------------------------------------------------------%
% aob reduction into canonical form
-spec aob2wf(aob_tree()) -> wf().
% If this node is a sum node, just reduce the left and the right
aob2wf({sum, Left, Right}) ->
    {wf, LeftTerms}  = aob2wf(Left),
    {wf, RightTerms} = aob2wf(Right),
    UnreducedTerms   = LeftTerms ++ RightTerms,
    NewTerms         = sum_of_baseprods(UnreducedTerms),
    {wf, NewTerms};
aob2wf({prod, Left, Right}) ->
    {wf, LeftTerms}  = aob2wf(Left),
    {wf, RightTerms} = aob2wf(Right),
    UnreducedTerms   = prod_of_sums_of_baseprods(LeftTerms, RightTerms),
    NewTerms         = sum_of_baseprods(UnreducedTerms),
    {wf, NewTerms};
aob2wf(BaseTerm = {baseprod, _Terms}) ->
    {wf, [BaseTerm]};
aob2wf(uno) ->
    {wf, [uno]}.


-spec atom2aob(atom()) -> baseprod().
atom2aob(uno) -> uno;
atom2aob(Foo) -> {baseprod, [Foo]}.

% Construct a baseprod
-spec baseprod(Atoms) -> baseprod()
    when Atoms :: nonempty_list(atom()).
baseprod(Atoms) ->
    Sorted            = lists:sort(Atoms),
    SortedUniq        = uniq(Sorted),
    SortedUniqCancel1 = eliminate_redundant_uno(SortedUniq),
    {baseprod, SortedUniqCancel1}.


% Construct a prod
-spec lprod(Trees) -> SumTrees
    when Trees    :: nonempty_list(Tree),
         SumTrees :: aob_tree(),
         Tree     :: aob_tree().
% Single element, just return it
lprod([Tree]) ->
    Tree;
% Two elements, combine them
lprod([TreeL, TreeR | Rest]) ->
    ThisTerm = {prod, TreeL, TreeR},
    NewList = [ThisTerm | Rest],
    lprod(NewList).


% Construct a sum
-spec lsum(Trees) -> SumTrees
    when Trees    :: nonempty_list(Tree),
         SumTrees :: aob_tree(),
         Tree     :: aob_tree().
% Single element, just return it
lsum([Tree]) ->
    Tree;
% Two elements, combine them
lsum([TreeL, TreeR | Rest]) ->
    ThisTerm = {sum, TreeL, TreeR},
    NewList = [ThisTerm | Rest],
    lsum(NewList).

% alias for aob2wf
-spec reduce(aob_tree()) -> wf().
reduce(X) -> aob2wf(X).


% alias for atom2aob
-spec sym(atom()) -> baseprod().
sym(X) -> atom2aob(X).


%-----------------------------------------------------------------------------%
% Internals
%-----------------------------------------------------------------------------%

% Product of two sums of baseprods (implements distributive property)
-spec prod_of_sums_of_baseprods(Baseprods1, Baseprods2) -> Baseprods
    when Baseprods1 :: [baseprod()],
         Baseprods2 :: [baseprod()],
         Baseprods  :: [baseprod()].
% If either list is empty (corresponds to 0), then the product is empty
% (corresponds to 0)
prod_of_sums_of_baseprods([], _) ->
    [];
prod_of_sums_of_baseprods(_, []) ->
    [];
% distribute the head term on the left over the terms on the right, concatenate
prod_of_sums_of_baseprods(Baseprods1, Baseprods2) ->
    % produces an arbitrary-depth list of lists
    Summandss = prod_of_sums_of_baseprods_with_accum(Baseprods1, Baseprods2, []),
    Summands  = lists:flatten(Summandss),
    Summands.


% Returns an arbitrary depth list
-spec prod_of_sums_of_baseprods_with_accum(Baseprods1, Baseprods2, Accum) -> Result
    when Baseprods1 :: [baseprod()],
         Baseprods2 :: [baseprod()],
         Accum      :: [AccumTerm],
         AccumTerm  :: baseprod()
                     | [AccumTerm],
         Result     :: Accum.
% When we run out of items to distribute on the left, return the accumulator
prod_of_sums_of_baseprods_with_accum([], _, Accum) ->
    Accum;
prod_of_sums_of_baseprods_with_accum([Baseprod | Rest], Baseprods2, Accum) ->
    LeadSums = distribute_baseprod_over_baseprods(Baseprod, Baseprods2),
    NewAccum = [LeadSums | Accum],
    prod_of_sums_of_baseprods_with_accum(Rest, Baseprods2, NewAccum).


% distribute a baseprod over a list of baseprods
-spec distribute_baseprod_over_baseprods(Baseprod, Baseprods) -> Baseprods
    when Baseprod  :: baseprod(),
         Baseprods :: [baseprod()].
distribute_baseprod_over_baseprods(Baseprod, Baseprods) ->
    Summandss = distribute_baseprod_over_baseprods_with_accum(Baseprod, Baseprods, []),
    Summands  = lists:flatten(Summandss),
    Summands.


-spec distribute_baseprod_over_baseprods_with_accum(Baseprod, Baseprods, Accum) -> Result
    when Baseprod  :: baseprod(),
         Baseprods :: [baseprod()],
         Accum     :: [baseprod()],
         Result    :: Accum.
% No more products to distribute over, return accumulator
distribute_baseprod_over_baseprods_with_accum(_Baseprod, [], Accum) ->
    Accum;
distribute_baseprod_over_baseprods_with_accum(BaseprodL, [BaseprodR | Rest], Accum) ->
    NewSummand = baseprod_times(BaseprodL, BaseprodR),
    NewAccum = [NewSummand | Accum],
    distribute_baseprod_over_baseprods_with_accum(BaseprodL, Rest, NewAccum).


-spec baseprod_times(BaseprodL, BaseprodR) -> Result
    when BaseprodL :: baseprod(),
         BaseprodR :: baseprod(),
         Result    :: baseprod().
% If one of the terms is 1, return the other
baseprod_times(uno, X) ->
    X;
baseprod_times(X, uno) ->
    X;
% Product of baseprods corresponds to taking a union of the terms that appear
% in the product
baseprod_times({baseprod, TermsL}, {baseprod, TermsR}) ->
    Union = atom_list_union(TermsL, TermsR),
    {baseprod, Union}.


% union of two lists of atoms
-spec atom_list_union([atom()], [atom()]) -> [atom()].
atom_list_union(AtomsL, AtomsR) ->
    SortedL = lists:sort(AtomsL),
    SortedR = lists:sort(AtomsR),
    Merged  = lists:merge(SortedL, SortedR),
    Merged.


% Reduce a list of baseprods mod 2
-spec sum_of_baseprods(Baseprods) -> Result
    when Baseprods :: [baseprod()],
         Result    :: [baseprod()].
sum_of_baseprods(Baseprods) ->
    sum_of_baseprods_with_accum(Baseprods, []).


% Reduce flat sum list to another flat sum list
-spec sum_of_baseprods_with_accum(Baseprods, Accum) -> Result
    when Baseprods :: [baseprod()],
         Accum     :: [baseprod()],
         Result    :: Accum.
sum_of_baseprods_with_accum([], Accum) ->
    Accum;
sum_of_baseprods_with_accum([Elem | Rest], Accum) ->
    NewAccum =
        case lists:member(Elem, Accum) of
            % Element is not in the list, add it
            false -> [Elem | Accum];
            % Element is in the list, remove it
            true  -> Accum -- [Elem]
        end,
    sum_of_baseprods_with_accum(Rest, NewAccum).


% Clone of unix uniq command
-spec uniq(Terms) -> Result
    when Terms  :: [T],
         Result :: [T],
         T      :: term().
% empty or singleton list, just return it
uniq([])  -> [];
uniq([X]) -> [X];
uniq([Head | Rest]) ->
    RevRest = uniq_with_accum(Rest, Head, []),
    NewRest = lists:reverse(RevRest),
    [Head | NewRest].


% Returns reversed list
-spec uniq_with_accum(Terms, LastElt, Accum) -> Accum
    when Terms   :: [T],
         LastElt :: T,
         Accum   :: [T],
         T       :: term().
% End of the list, return accumulator
uniq_with_accum([], _, Accum) ->
    Accum;
% if duplicate, continue
uniq_with_accum([X | Rest], X, Accum) ->
    uniq_with_accum(Rest, X, Accum);
% if not duplicate, add to stack and continue
uniq_with_accum([X | Rest], _, Accum) ->
    uniq_with_accum(Rest, X, [X | Accum]).


-spec eliminate_redundant_uno(Atoms) -> Atoms
    when Atoms :: nonempty_list(atom()).
% If the list contains only 'uno's, return [uno]
% otherwise return everything that isn't 'uno'
eliminate_redundant_uno(Atoms) ->
    case entire_list_is_uno(Atoms) of
        true  -> [uno];
        false -> eliminate_all_uno(Atoms)
    end.


-spec entire_list_is_uno(Atoms) -> boolean()
    when Atoms :: nonempty_list(atom()).
% if list is just one element check if its uno
entire_list_is_uno([uno]) ->
    true;
entire_list_is_uno([_NotUno]) ->
    false;
% if list is more one element check if head is uno
entire_list_is_uno([uno | Rest]) ->
    entire_list_is_uno(Rest);
entire_list_is_uno([_NotUno | _Rest]) ->
    false.

% eliminate every item in the list that is called 'uno'
-spec eliminate_all_uno([atom()]) -> [atom()].
eliminate_all_uno(Atoms) ->
    [Item || Item <- Atoms, Item =/= 'uno'].
