#!/usr/bin/env escript

-mode(compile).
-compile(nowarn_unused_function).
-compile(nowarn_unused_variable).

% Try out the truth table for 2nd digit of binary adder
main(_) ->
    c4().

% v2
v2() ->
    Uno = wf:sym(uno),
    C2  = wf:sym(c2),
    A2  = wf:sym('A2'),
    B2  = wf:sym('B2'),
    NC2 = wf:lsum([Uno, C2]),
    NA2 = wf:lsum([Uno, A2]),
    NB2 = wf:lsum([Uno, B2]),
    BaseTree =
        wf:lsum(
            [ wf:lprod([NC2, NA2,  B2])
            , wf:lprod([NC2,  A2, NB2])
            , wf:lprod([ C2, NA2, NB2])
            , wf:lprod([ C2,  A2,  B2])
            ]
        ),
    %ok = io:format("~p~n", [BaseTree]),
    WFTree = wf:reduce(BaseTree),
    ok = io:format("~p~n", [WFTree]).


% c4: carry for the 4s place
c4() ->
    Uno = wf:sym(uno),
    C2  = wf:sym(c2),
    A2  = wf:sym('A2'),
    B2  = wf:sym('B2'),
    NC2 = wf:lsum([Uno, C2]),
    NA2 = wf:lsum([Uno, A2]),
    NB2 = wf:lsum([Uno, B2]),
    BaseTree =
        wf:lsum(
            [ wf:lprod([NC2,  A2,  B2])
            , wf:lprod([ C2, NA2,  B2])
            , wf:lprod([ C2,  A2, NB2])
            , wf:lprod([ C2,  A2,  B2])
            ]
        ),
    %ok = io:format("~p~n", [BaseTree]),
    WFTree = wf:reduce(BaseTree),
    ok = io:format("~p~n", [WFTree]).

% v4: value for the 4s place
v4() ->
    Uno = wf:sym(uno),
    C2  = wf:sym(c2),
    A2  = wf:sym('A2'),
    B2  = wf:sym('B2'),
    NC2 = wf:lsum([Uno, C2]),
    NA2 = wf:lsum([Uno, A2]),
    NB2 = wf:lsum([Uno, B2]),
    BaseTree =
        wf:lsum(
            [ wf:lprod([NC2,  A2,  B2])
            , wf:lprod([ C2, NA2,  B2])
            , wf:lprod([ C2,  A2, NB2])
            , wf:lprod([ C2,  A2,  B2])
            ]
        ),
    %ok = io:format("~p~n", [BaseTree]),
    WFTree = wf:reduce(BaseTree),
    ok = io:format("~p~n", [WFTree]).
