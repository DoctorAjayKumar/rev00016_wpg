% binary dataframes
-module(bdf).

-export(
    [ read_file/1
    ]
).

-record(col,
        { name :: term()
        , type :: param | fn
        }).
-type col() :: #col{} | pipe.

%-----------------------------------------------------------------------------%
% API
%-----------------------------------------------------------------------------%

-spec read_file(Filename) -> ok
    when Filename :: file:name_all().
read_file(Filename) ->
    {ok, Device} = file:open(Filename, [read]),
    Cols = read_header(Device),
    io:format("~p~n", [Cols]).

%-----------------------------------------------------------------------------%
% Internal
%-----------------------------------------------------------------------------%

-spec read_header(Device) -> Columns
    when
        Device  :: file:io_device(),
        Columns :: [binary()].
read_header(Device) ->
    {ok, FirstLine} = file:read_line(Device),
    FLChomp = string:chomp(FirstLine),
    ColNames = string:lexemes(FLChomp, ","),
    Cols = atoms2cols(ColNames),
    Cols.


-spec atoms2cols(ColNames) -> Cols
    when
        ColNames :: [string()],
        Cols     :: [col()].
atoms2cols(Strs) ->
    RevLs = atoms2cols3(Strs, no_pipe_yet, []),
    lists:reverse(RevLs).


-spec atoms2cols3(ColNames, MaybePipeYet, Accum) -> Cols
    when
        ColNames     :: [string()],
        MaybePipeYet :: no_pipe_yet | pipe_yet,
        Cols         :: [col()],
        Accum        :: Cols.
atoms2cols3([], _, Accum) ->
    Accum;
atoms2cols3(["|" | Rest], _, Accum) ->
    atoms2cols3(Rest, pipe_yet, [pipe | Accum]);
atoms2cols3([Name | Rest], no_pipe_yet, Accum) ->
    Col = #col{name=Name, type=param},
    NewAccum = [Col | Accum],
    atoms2cols3(Rest, no_pipe_yet, NewAccum);
atoms2cols3([Name | Rest], pipe_yet, Accum) ->
    Col = #col{name=Name, type=fn},
    NewAccum = [Col | Accum],
    atoms2cols3(Rest, pipe_yet, NewAccum).
